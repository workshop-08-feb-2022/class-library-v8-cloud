﻿using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace SampleDemo.Core.Controllers
{
    public class TeaController : RenderMvcController
    {
        public ActionResult Index(ContentModel model)
        {
            Logger.Info<TeaController>($"{model.Content.Id} value of sugar is {Request.Params["sugar"]}");
            return CurrentTemplate(model);

        }

    }
}
